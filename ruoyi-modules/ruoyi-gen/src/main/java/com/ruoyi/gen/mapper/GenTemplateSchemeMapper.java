package com.ruoyi.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.gen.domain.GenTemplateScheme;

import java.util.List;

/**
 * 代码生成模板组管理Mapper接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface GenTemplateSchemeMapper extends BaseMapper<GenTemplateScheme> {

    /**
     * 查询代码生成模板组管理列表
     *
     * @param genTemplateScheme 代码生成模板组管理
     * @return 代码生成模板组管理集合
     */
    List<GenTemplateScheme> selectGenTemplateSchemeList(GenTemplateScheme genTemplateScheme);


}
